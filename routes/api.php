<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CategoriesController;
use App\Http\Controllers\Api\ProductsController;
use App\Http\Controllers\Api\ReviewsController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//auth
Route::post('auth/login',[AuthController::class ,'login']);
Route::post('auth/register',[AuthController::class ,'register']);

//products
Route::get('/products',[ProductsController::class,'index']);
Route::get('/products/{product}',[ProductsController::class,'show']);

//categories
Route::get('/categories',[CategoriesController::class,'index']);
Route::get('/categories/{category}',[CategoriesController::class,'show']);

//reviews
Route::get('/products/{product}/reviews',[ReviewsController::class , 'index']);

//authed paths
    //my account
    Route::group(['middleware'=>'auth:api' ],function(){
        //admin
        Route::group(['middleware'=>'is_admin' ],function(){
        Route::post('/products',[ProductsController::class,'store']);
        });
        Route::group (['prefix'=>'profile'],function(){
            Route::get('/',[UserController::class , 'index']);
            Route::put('/',[UserController::class , 'update']);

        });
        //reviews
        Route::post('/products/{product}/reviews',[ReviewsController::class ,'store']);
    });

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
