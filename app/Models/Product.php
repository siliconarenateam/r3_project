<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Category;
use App\Models\ProductVarient;
use App\Models\ProductImage;
use App\Models\ProductReview;
use App\Models\ProductSpec;

class Product extends Model
{
    use HasFactory;

    public function category(){

        return $this->belongsTo(Category::class,'category_id');
    }
    public function varients()
    {
    return $this->hasMany(ProductVarient::class,'product_id');
    }
    public function images()
    {
        return $this->hasMany(ProductImage::class,'product_id');
    }
    public function reviews()
    {
        return $this->hasMany(ProductReview::class,'product_id');
    }
    public function specs()
    {
        return $this->hasMany(ProductSpec::class,'product_id');
    }
}
