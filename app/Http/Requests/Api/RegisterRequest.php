<?php

namespace App\Http\Requests\Api;

use App\Http\Misc\Helpers\Base64Handler;
use App\Rules\ValidEncodedFile;
use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'email'=>'required|max:200|unique:users,email',
            'password'=>'required|min:5|max:200|confirmed',
            'name'=>'required|min:3|max:30',
            'avatar'=> ['required', new ValidEncodedFile(Base64Handler::IMAGE_EXTS)]

        ];
    }
}
