<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\Base64Handler;
use App\Http\Requests\Api\LoginRequest;
use App\Http\Requests\Api\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    //
    public function login (LoginRequest $request){

        if(!Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            return ["message"=>'email or password not registered'];
        }
        else{
            $user = User::where('email',strtolower($request->email))->first();
            $token = $user->createToken('backend')->accessToken;
            return ["user"=>$user ,"token"=>$token];

        }

    }
    public function register (RegisterRequest $request){

        $user = User::create(["email"=>$request->email,'name'=>$request->name,'password'=>bcrypt($request->password)
        ,'avatar'=>Base64Handler::storeFile($request->avatar,'users_images')
        ]);
        $token = $user->createToken('backend')->accessToken;
        return ["user"=>$user ,"token"=>$token];
    }

}
