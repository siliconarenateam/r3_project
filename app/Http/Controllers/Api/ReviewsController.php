<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateReviewRequest;
use App\Http\Resources\Api\ReviewResource;
use App\Models\Product;
use App\Models\ProductReview;
use Illuminate\Http\Request;

class ReviewsController extends Controller
{
    //
    public function index (Request $request ,Product $product){

         $reviews= $product->reviews()->paginate();
         return ReviewResource::collection($reviews);
    }
    public function store (CreateReviewRequest $request ,Product $product){


        $review = new ProductReview();
        $review->product_id= $product->id;
        $review->user_id= $request->user()->id;
        $review->review= $request->review;
        $review->rate= $request->rate;
        $review->save();
        return  new ReviewResource($review);
        // $reviews= $product->reviews()->paginate();
        // return ReviewResource::collection($reviews);
   }
}
