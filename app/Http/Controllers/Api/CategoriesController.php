<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Api\CategoryResource;
use App\Http\Resources\Api\MiniProductResource;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    //
    public function index(){
        $items=Category::paginate();
        return CategoryResource::collection($items);
   }
   public function show(Request $request ,Category $category){
       $products= $category->products()->paginate();
       return  MiniProductResource::collection( $products);
   }
}
