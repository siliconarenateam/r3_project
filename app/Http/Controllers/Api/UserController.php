<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Misc\Helpers\Base64Handler;
use App\Http\Requests\Api\UpdateUserRequest;
use App\Http\Resources\Api\UserResource;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //

    public function index (Request $request) {
        $user = $request->user();
        return new UserResource($user);
    }
    public function update (UpdateUserRequest $request) {

        $user = $request->user();
        if($request->name) $user->name = $request->name;
        //delete old image (future)
        //uploade
        if($request->avatar) {
            $user->avatar=Base64Handler::storeFile($request->avatar,'users_images') ;
        }
        $user->save();
        return new UserResource($user);
    }
}
