<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateProductRequest;
use App\Http\Resources\Api\MiniProductResource;
use App\Http\Resources\Api\ProductResource;
use App\Models\Product;
use App\Models\ProductVarient;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    //
    public function index(Request $request){
         $products=Product::where('status','=',1);

         //optional request param
         if($request->has('min_price') && $request->has('max_price'))
         {
             $products=$products->whereBetween('price',[$request->min_price,$request->max_price]);
         }

         $products=$products->paginate();


         return MiniProductResource::collection($products);
    }
    public function show(Request $request ,Product $product){
        return new ProductResource($product);
    }
    public function store(CreateProductRequest $request ){
        //store new product
        $product = new Product();
        $product->title = $request->title;
        $product->category_id = $request->category_id;
        $product->description = $request->description;
        $product->stock = $request->stock;
        $product->status = $request->status;
        $product->stock = $request->stock;
        $product->price = $request->price;
        $product->save();
        //create vairents
        foreach($request->varients  as $item ){
            $varient = new ProductVarient();
            $varient->title = $request->title."-".$item['color']."-".$item['size'];
            $varient->product_id = $product->id;
            $varient->description = $request->description;
            $varient->stock = $request->stock;
            $varient->status = $request->status;
            $varient->stock = $request->stock;
            $varient->price = $item['price'];
            $varient->color = $item['color'];
            $varient->size = $item['size'];
            $varient->save();
        }

        return new ProductResource($product);
    }

}
