<?php

namespace App\Http\Resources\Api;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Api\MiniProductResource;
class ReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return
        [
            'id'=>$this->id,
            'product'=>new MiniProductResource($this->product),
            'review'=>$this->review,
            'rate'=>$this->rate,
            'created_at'=>Carbon::parse($this->created_at)->diffForHumans()

        ];
    }
}
