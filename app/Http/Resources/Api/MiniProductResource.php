<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;

class MiniProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return          [
            'id'            =>$this->id,
            'category_id'   =>$this->category_id,
            'category_name' =>$this->category->name,
            'stock'         =>$this->stock,
            'status'        =>$this->status,
            'title'         =>$this->title,
            'price'         =>$this->price,
            'desc'          =>$this->description,
            'image'        =>$this->images()->first(),
            'reviews_count' =>$this->reviews->count(),


        ];;
    }
}
