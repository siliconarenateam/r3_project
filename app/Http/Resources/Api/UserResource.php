<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        =>$this->id,
            'name'      =>$this->name,
            "email"     =>$this->email,
            "avatar"    =>asset('files/users/images/'.$this->avatar),
            "reviews_count" =>$this->reviews()->count()

        ];
    }
}
