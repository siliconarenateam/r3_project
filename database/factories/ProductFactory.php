<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $category_id=Category::limit(10)->inRandomOrder()->implode('id')->first();
        return [
            //
            'title'=>$this->faker->text(220),
            'description'=>$this->faker->sentence(100,true),
            'category_id'=>$category_id,
            'status'=>rand(1,3),
            'stock'=>rand(0,100),
            'price'=>rand(50,10000),
            'has_varient'=>rand(1,2)

        ];
    }
}
